import { TodoNg2Page } from './app.po';

describe('todo-ng2 App', function() {
  let page: TodoNg2Page;

  beforeEach(() => {
    page = new TodoNg2Page();
  });

  it('should display title saying todos', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('todos');
  });
});
