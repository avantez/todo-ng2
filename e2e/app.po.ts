import { browser, element, by } from 'protractor';

export class TodoNg2Page {
  navigateTo() {
    return browser.get('/');
  }

  getTitleText() {
    return element(by.css('app-root main h1')).getText();
  }
}
