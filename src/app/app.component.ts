import { Component } from '@angular/core';
import { Todo } from './shared';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  appTitle: string = 'todos';

  newTodoTitle: string = '';
  todos: Todo[] = [];

  addTodo() {
    if (this.newTodoTitle) {
      this.todos.push(new Todo(this.newTodoTitle));
      this.newTodoTitle = '';
    }
  }
}
