import { async } from '@angular/core/testing';
import { Todo } from './todo.model';

describe('Todo Model', () => {
  it('should proper create a todo with a title', async(() => {
    let title = 'new todo';
    let todo = new Todo(title);
    expect(todo).toBeTruthy();
    expect(todo.title).toBe(title);
  }));

  it('should proper set a new title for todo', async(() => {
    let todo = new Todo('');
    let newTitle = 'changed title';
    todo.title = newTitle;
    expect(todo.title).toBe(newTitle);
  }));
});
