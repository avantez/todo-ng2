/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

describe('App: TodoNg2', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        FormsModule
      ]
    });
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'todos'`, async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app.appTitle).toEqual('todos');
  }));

  it('should render title in a h1 tag', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('main h1').textContent).toContain('todos');
  }));

  it('should not add todo when newTitle is an empty string', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    let beforeLength = app.todos.length;
    expect(app.newTodoTitle).toBe('');
    app.addTodo();
    expect(app.todos.length).toBe(beforeLength);
  }));

  it('should add the todo on the list', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    let todoTitle = 'new';
    let beforeLength = app.todos.length;
    app.newTodoTitle = todoTitle;
    expect(app.newTodoTitle).toBe(todoTitle);
    app.addTodo();
    expect(app.todos.length).toBe(beforeLength + 1);
    expect(app.todos[app.todos.length - 1].title).toBe(todoTitle);
  }));
});
